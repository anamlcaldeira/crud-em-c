namespace CRUD_Ana_Maria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Clientes : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Clientes");
            AddColumn("dbo.Clientes", "ClienteId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Clientes", "ClienteId");
            DropColumn("dbo.Clientes", "ClientId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Clientes", "ClientId", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Clientes");
            DropColumn("dbo.Clientes", "ClienteId");
            AddPrimaryKey("dbo.Clientes", "ClientId");
        }
    }
}
