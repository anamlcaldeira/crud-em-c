﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_Ana_Maria.Models
{
    public class Cliente
    {
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public Int64 ClientCPF { get; set; }
        public DateTime ClientDateRegister { get; set; }
        public string ClientComments { get; set; }
    }
}
