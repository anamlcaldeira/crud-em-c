﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CRUD_Ana_Maria.Models;
using System.Data.SqlClient;

namespace CRUD_Ana_Maria
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            var context = new Contexto();
            context.Cliente.Add(new Cliente
            {
                ClientId = 1,
                ClientCPF = 123456,
                ClientName = "Ana Maria",
                ClientComments = "Exemplo",
                ClientDateRegister = DateTime.Now
            });
            context.SaveChanges();
         }
    }
}
